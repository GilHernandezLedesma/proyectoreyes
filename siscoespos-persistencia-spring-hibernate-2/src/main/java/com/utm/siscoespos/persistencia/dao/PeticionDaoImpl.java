package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Peticion;

@Named
public class PeticionDaoImpl implements PeticionDao {
	
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Peticion p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Peticion p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Peticion p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Peticion obtenerPorId(Integer id) {		
		Peticion perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Peticion p where p.id=:id");
		query.setParameter("id", id);
		perro =(Peticion) query.uniqueResult();
		return perro;
	}

	@Override
	public List<Peticion> obtener() {
		List<Peticion> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Peticion p");		
		lista = query.list();
		return lista;
	}

}
