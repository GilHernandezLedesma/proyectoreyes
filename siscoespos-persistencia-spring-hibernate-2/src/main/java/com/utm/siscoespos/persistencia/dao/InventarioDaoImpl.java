package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Inventario;

@Named
public class InventarioDaoImpl implements InventarioDao {
	
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Inventario p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Inventario p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Inventario p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Inventario obtenerPorId(Integer id) {		
		Inventario perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Inventario p where p.id=:id");
		query.setParameter("id", id);
		perro =(Inventario) query.uniqueResult();
		return perro;
	}

	@Override
	public List<Inventario> obtener() {
		List<Inventario> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Inventario p");		
		lista = query.list();
		return lista;
	}

}
