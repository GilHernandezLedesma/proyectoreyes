package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.NinoDao;
import com.utm.siscoespos.persistencia.dominio.Nino;
@Transactional
@Named
public class NinoServicioImpl implements NinoServicio {
	@Inject
	NinoDao ninoDao;
	
	@Override
	public void crear(Nino p) {
		ninoDao.crear(p);
	}

	@Override
	public void modificar(Nino p) {
		ninoDao.modificar(p);
	}

	@Override
	public void eliminar(Nino p) {
		ninoDao.eliminar(p);
	}

	@Override
	public Nino obtenerPorId(Integer id) {
		return ninoDao.obtenerPorCodigo(id);
	}

	@Override
	public List<Nino> obtener() {
		return ninoDao.obtener();
	}

}
