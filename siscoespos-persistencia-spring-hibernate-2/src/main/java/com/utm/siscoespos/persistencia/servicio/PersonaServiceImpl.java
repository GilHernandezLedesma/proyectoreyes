package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.PersonaDao;
import com.utm.siscoespos.persistencia.dominio.Persona;

@Transactional
@Named
public class PersonaServiceImpl implements PersonaService {

	@Inject
	private PersonaDao personaDao;

	@Override
	public List<Persona> obtenerPersonas() {
		return personaDao.obtenerPersonas();
	}
	
	@Override
	public Persona obtenerPersonaPorId(Integer idPersona) {
		return personaDao.obtenerPersonaPorId(idPersona);
	}

	
	@Override
	public void actualizarPersona(Persona persona) {			
		personaDao.actualizarPersona(persona);
	}

	@Override
	public void crearPersona(Persona persona) {
		personaDao.crearPersona(persona);
	}

	@Override
	public void eliminarPersona(Persona persona) {
		personaDao.eliminarPersona(persona);
	}


}
