package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Carta;

public interface CartaDao {
	public void crear(Carta p);
	public void modificar(Carta p);
	public void eliminar(Carta p);
	public Carta obtenerPorId(Integer id);
	public List<Carta> obtenerPorFormaEnvio(String formaEnvio);
}
