package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Historial;

public interface HistorialServicio {
	public void crear(Historial p);
	public void modificar(Historial p);
	public void eliminar(Historial p);
	public Historial obtenerPorId(Integer id);
	public List<Historial> obtener();
}
