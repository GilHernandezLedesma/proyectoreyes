package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Nino {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nombre;
	private Date fechaNacimiento;
	private String direccion;
	
	@ManyToOne
	@JoinColumn(name = "poblacion_id")
	private Poblacion poblacion;
	
	@OneToMany(
			mappedBy = "nino", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Historial> historiales = new ArrayList<Historial>();


	public void addHistorial(Historial h) {
		historiales.add(h);
		h.setNino(this);
	}
	public void removeHistorial(Historial h){
		historiales.remove(h);
		h.setNino(null);
	}
	

	
	public Nino() {
	}
	public Nino(Nino n) {
		this.id = n.id;
		this.nombre = n.nombre;
		this.fechaNacimiento=n.fechaNacimiento;
		//this.direccion=n.direccion;
		//this.poblacion=n.poblacion;
		//this.historiales=n.historiales;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Poblacion getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(Poblacion poblacion) {
		this.poblacion = poblacion;
	}
	public List<Historial> getHistoriales() {
		return historiales;
	}
	public void setHistoriales(List<Historial> historiales) {
		this.historiales = historiales;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
}
