package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.PeticionDao;
import com.utm.siscoespos.persistencia.dominio.Peticion;
@Transactional
@Named
public class PeticionServicioImpl implements PeticionServicio {
	@Inject
	PeticionDao peticionDao;
	
	@Override
	public void crear(Peticion p) {
		peticionDao.crear(p);
	}

	@Override
	public void modificar(Peticion p) {
		peticionDao.modificar(p);
	}

	@Override
	public void eliminar(Peticion p) {
		peticionDao.eliminar(p);
	}

	@Override
	public Peticion obtenerPorId(Integer id) {
		return peticionDao.obtenerPorId(id);
	}

	@Override
	public List<Peticion> obtener() {
		return peticionDao.obtener();
	}

}
