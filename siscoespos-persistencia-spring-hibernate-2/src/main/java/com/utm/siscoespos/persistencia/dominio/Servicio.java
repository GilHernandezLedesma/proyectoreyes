package com.utm.siscoespos.persistencia.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Servicio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "poblacion_id")
	private Poblacion poblacion;
	
	@ManyToOne
	@JoinColumn(name = "almacen_id")
	private Almacen almacen;

	
	public Servicio() {
		// TODO Auto-generated constructor stub
	}
	public Servicio(Servicio s) {
		this.id = s.id;
		//this.poblacion = s.poblacion;
		//this.almacen=s.almacen;
	}

	//Sets and gets
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Poblacion getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(Poblacion poblacion) {
		this.poblacion = poblacion;
	}

	public Almacen getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}
	
	
	
	
}
