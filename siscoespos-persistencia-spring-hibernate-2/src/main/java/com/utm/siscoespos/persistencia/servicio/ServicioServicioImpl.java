package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.ServicioDao;
import com.utm.siscoespos.persistencia.dominio.Servicio;
@Transactional
@Named
public class ServicioServicioImpl implements ServicioServicio {
	@Inject
	ServicioDao servicioDao;
	
	@Override
	public void crear(Servicio p) {
		servicioDao.crear(p);
	}

	@Override
	public void modificar(Servicio p) {
		servicioDao.modificar(p);
	}

	@Override
	public void eliminar(Servicio p) {
		servicioDao.eliminar(p);
	}

	@Override
	public Servicio obtenerPorId(Integer id) {
		return servicioDao.obtenerPorId(id);
	}

	@Override
	public List<Servicio> obtener() {
		return servicioDao.obtener();
	}

}
