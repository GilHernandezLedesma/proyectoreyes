package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Asignacion;

@Named
public class AsignacionDaoImpl implements AsignacionDao {
	
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Asignacion p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Asignacion p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Asignacion p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Asignacion obtenerPorId(Integer id) {		
		Asignacion perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Asignacion p where p.id=:id");
		query.setParameter("id", id);
		perro =(Asignacion) query.uniqueResult();
		return perro;
	}

	@Override
	public List<Asignacion> obtener() {
		List<Asignacion> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Asignacion p");		
		lista = query.list();
		return lista;
	}

}
