package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.CartaDao;
import com.utm.siscoespos.persistencia.dominio.Carta;
@Transactional
@Named
public class CartaServicioImpl implements CartaServicio {
	@Inject
	CartaDao cartaDao;
	
	@Override
	public void crear(Carta p) {
		cartaDao.crear(p);
	}

	@Override
	public void modificar(Carta p) {
		cartaDao.modificar(p);
	}

	@Override
	public void eliminar(Carta p) {
		cartaDao.eliminar(p);
	}

	@Override
	public Carta obtenerPorId(Integer id) {
		return cartaDao.obtenerPorId(id);
	}

	@Override
	public List<Carta> obtenerPorFormaEnvio(String formaEnvio) {
		return cartaDao.obtenerPorFormaEnvio(formaEnvio);
	}


}
