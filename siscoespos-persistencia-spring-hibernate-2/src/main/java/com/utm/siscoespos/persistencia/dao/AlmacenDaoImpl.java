package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Almacen;

@Named
public class AlmacenDaoImpl implements AlmacenDao{
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Almacen p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Almacen p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Almacen p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}
	
	@Override
	public void eliminarPorId(Integer p) {
		//Session session = sessionFactory.getCurrentSession();
		//session.delete(p);
	}

	@Override
	public Almacen obtenerPorId(Integer id) {		
		Almacen perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Almacen p where p.id=:id");
		query.setParameter("id", id);
		perro =(Almacen) query.uniqueResult();
		return perro;
	}

	@Override
	public List<Almacen> obtener() {
		List<Almacen> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Almacen p");		
		lista = query.list();
		return lista;
	}
}
