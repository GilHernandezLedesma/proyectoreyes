package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.dominio.Perro;

@Named
public class CartaDaoImpl implements CartaDao {

	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Carta p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Carta p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Carta p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Carta obtenerPorId(Integer id) {
		Carta carta = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Carta p where p.id=:id");
		query.setParameter("id", id);
		carta =(Carta) query.uniqueResult();
		return carta;
	}
	
	@Override
	public List<Carta> obtenerPorFormaEnvio(String formaEnvio) {
		List<Carta> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Perro p where p.formaEnvio=:formaEnvio");
		query.setParameter("formaEnvio", formaEnvio);
		lista = query.list();
		return lista;
	}


}
