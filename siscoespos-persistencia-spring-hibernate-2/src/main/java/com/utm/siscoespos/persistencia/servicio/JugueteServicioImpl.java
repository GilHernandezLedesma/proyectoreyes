package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.JugueteDao;
import com.utm.siscoespos.persistencia.dominio.Juguete;
@Transactional
@Named
public class JugueteServicioImpl implements JugueteServicio {
	@Inject
	JugueteDao jugueteDao;
	
	@Override
	public void crear(Juguete p) {
		jugueteDao.crear(p);
	}

	@Override
	public void modificar(Juguete p) {
		jugueteDao.modificar(p);
	}

	@Override
	public void eliminar(Juguete p) {
		jugueteDao.eliminar(p);
	}

	@Override
	public Juguete obtenerPorId(String id) {
		return jugueteDao.obtenerPorId(id);
	}
	
	@Override
	public Juguete obtenerPrimero() {
		return jugueteDao.obtenerPrimero();
	}

	@Override
	public List<Juguete> obtener() {
		return jugueteDao.obtener();
	}

}
