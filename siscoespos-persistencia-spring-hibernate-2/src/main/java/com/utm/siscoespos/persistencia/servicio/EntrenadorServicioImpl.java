package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.EntrenadorDao;
import com.utm.siscoespos.persistencia.dominio.Entrenador;
@Transactional
@Named
public class EntrenadorServicioImpl implements EntrenadorServicio {
	@Inject
	EntrenadorDao entrenadorDao;
	
	@Override
	public void crear(Entrenador p) {
		entrenadorDao.crear(p);
	}

	@Override
	public void modificar(Entrenador p) {
		entrenadorDao.modificar(p);
	}

	@Override
	public void eliminar(Entrenador p) {
		entrenadorDao.eliminar(p);
	}

	@Override
	public Entrenador obtenerPorId(Integer id) {
		return entrenadorDao.obtenerPorId(id);
	}

	@Override
	public List<Entrenador> obtener() {
		return entrenadorDao.obtener();
	}

}
