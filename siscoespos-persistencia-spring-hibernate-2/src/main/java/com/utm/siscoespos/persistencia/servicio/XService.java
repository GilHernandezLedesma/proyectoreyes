package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.X;



public interface XService {
	
	List<X> obtenerXs();
	
	X obtenerXPorId(Integer idX);
	
	void actualizarX(X persona);	
	
	void crearX(X persona);
	
	void eliminarX(X persona);
}
