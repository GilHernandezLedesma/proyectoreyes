package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Entrenador;

public interface EntrenadorServicio {
	public void crear(Entrenador p);
	public void modificar(Entrenador p);
	public void eliminar(Entrenador p);
	public Entrenador obtenerPorId(Integer id);
	public List<Entrenador> obtener();
}
