package com.utm.siscoespos.persistencia.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Asignacion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer cantidad;
	
	@ManyToOne
	@JoinColumn(name = "historial_id")
	private Historial historial;
	
	@ManyToOne
	@JoinColumn(name = "juguete_id")
	private Juguete juguete;

	
	
	public Asignacion() {
		// TODO Auto-generated constructor stub
	}
	public Asignacion(Asignacion a) {
		this.id = a.id;
		this.cantidad=a.cantidad;
		//this.historial=a.historial;
		//this.juguete=a.juguete;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Historial getHistorial() {
		return historial;
	}

	public void setHistorial(Historial historial) {
		this.historial = historial;
	}

	public Juguete getJuguete() {
		return juguete;
	}

	public void setJuguete(Juguete juguete) {
		this.juguete = juguete;
	}
	
	

}
