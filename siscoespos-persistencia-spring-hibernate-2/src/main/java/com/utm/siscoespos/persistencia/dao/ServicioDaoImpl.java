package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Perro;
import com.utm.siscoespos.persistencia.dominio.Servicio;

@Named
public class ServicioDaoImpl implements ServicioDao {

	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Servicio s) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(s);
	}

	@Override
	public void modificar(Servicio s) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(s);
	}

	@Override
	public void eliminar(Servicio s) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(s);
	}

	@Override
	public Servicio obtenerPorId(Integer id) {
		Servicio servicio = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Servicio s where s.id=:id");
		query.setParameter("id", id);
		servicio =(Servicio) query.uniqueResult();
		return servicio;
	}

	@Override
	public List<Servicio> obtener() {
		List<Servicio> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Servicio s");		
		lista = query.list();
		return lista;
	}

}
