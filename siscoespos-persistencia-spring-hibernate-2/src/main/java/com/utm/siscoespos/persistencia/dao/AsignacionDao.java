package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Asignacion;

public interface AsignacionDao {
	public void crear(Asignacion p);
	public void modificar(Asignacion p);
	public void eliminar(Asignacion p);
	public Asignacion obtenerPorId(Integer id);
	public List<Asignacion> obtener();
}
