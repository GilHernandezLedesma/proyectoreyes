package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Nino;

public interface NinoDao {
	public void crear(Nino n);
	public void modificar(Nino n);
	public void eliminar(Nino n);
	public Nino obtenerPorCodigo(Integer id);
	public List<Nino> obtener();
}
