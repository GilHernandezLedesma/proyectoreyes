package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.AsignacionDao;
import com.utm.siscoespos.persistencia.dominio.Asignacion;
@Transactional
@Named
public class AsignacionServicioImpl implements AsignacionServicio {
	@Inject
	AsignacionDao asignacionDao;
	
	@Override
	public void crear(Asignacion p) {
		asignacionDao.crear(p);
	}

	@Override
	public void modificar(Asignacion p) {
		asignacionDao.modificar(p);
	}

	@Override
	public void eliminar(Asignacion p) {
		asignacionDao.eliminar(p);
	}

	@Override
	public Asignacion obtenerPorId(Integer id) {
		return asignacionDao.obtenerPorId(id);
	}

	@Override
	public List<Asignacion> obtener() {
		return asignacionDao.obtener();
	}

}
