package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Juguete;

public interface JugueteDao {
	public void crear(Juguete p);
	public void modificar(Juguete p);
	public void eliminar(Juguete p);
	public Juguete obtenerPorId(String id);
	public Juguete obtenerPrimero();
	public List<Juguete> obtener();
}
