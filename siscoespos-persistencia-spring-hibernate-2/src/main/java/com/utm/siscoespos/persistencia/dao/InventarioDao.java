package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Inventario;

public interface InventarioDao {
	public void crear(Inventario p);
	public void modificar(Inventario p);
	public void eliminar(Inventario p);
	public Inventario obtenerPorId(Integer id);
	public List<Inventario> obtener();
}
