package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Persona;



public interface PersonaService {
	
	List<Persona> obtenerPersonas();
	
	Persona obtenerPersonaPorId(Integer idPersona);
	
	void actualizarPersona(Persona persona);	
	
	void crearPersona(Persona persona);
	
	void eliminarPersona(Persona persona);
}
