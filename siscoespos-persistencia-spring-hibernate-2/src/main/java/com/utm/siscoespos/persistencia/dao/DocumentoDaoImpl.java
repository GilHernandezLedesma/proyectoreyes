package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.utm.siscoespos.persistencia.dominio.Documento;


@Named
public class DocumentoDaoImpl implements DocumentoDao {

	@Inject
	private SessionFactory sessionFactory;

	@Override
	public List<Documento> obtenerDocumentos() {
		List<Documento> lista = null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Documento");
		lista =query.list();
		return lista;
	}
	
	@Override
	public Documento obtenerDocumentoPorId(Integer idDocumento) {
		Documento documento = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Documento d where d.idDocumento=:idDocumento");
		query.setParameter("idDocumento", idDocumento);
		documento =(Documento) query.uniqueResult();
		return documento;
	}

	
	@Override
	public void actualizarDocumento(Documento Documento) {
			
		Session session = sessionFactory.getCurrentSession();
		
		session.merge(Documento);
	}

	@Override
	public void crearDocumento(Documento Documento) {
		
		Session session = sessionFactory.getCurrentSession();

		session.persist(Documento);
	}

	@Override
	public void eliminarDocumento(Documento Documento) {
		
		Session session = sessionFactory.getCurrentSession();
		session.delete(Documento);
	}


}
