package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Perro;

public interface PerroDao {
	public void crear(Perro p);
	public void modificar(Perro p);
	public void eliminar(Perro p);
	public Perro obtenerPorId(Integer id);
	public List<Perro> obtener();
}
