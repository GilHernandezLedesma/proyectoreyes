package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.dominio.Perro;

@Named
public class NinoDaoImpl implements NinoDao {

	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Nino n) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(n);
	}

	@Override
	public void modificar(Nino n) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(n);
	}

	@Override
	public void eliminar(Nino n) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(n);
	}

	@Override
	public Nino obtenerPorCodigo(Integer id) {
		Nino nino = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Nino n where n.id=:id");
		query.setParameter("id", id);
		nino =(Nino) query.uniqueResult();
		return nino;
	}

	@Override
	public List<Nino> obtener() {
		List<Nino> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Nino n");		
		lista = query.list();
		return lista;
	}

}
