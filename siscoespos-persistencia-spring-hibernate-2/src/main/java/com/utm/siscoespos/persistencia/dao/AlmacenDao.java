package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Almacen;

public interface AlmacenDao {
	public void crear(Almacen p);
	public void modificar(Almacen p);
	public void eliminar(Almacen p);
	public void eliminarPorId(Integer id);
	public Almacen obtenerPorId(Integer id);
	public List<Almacen> obtener();
}
