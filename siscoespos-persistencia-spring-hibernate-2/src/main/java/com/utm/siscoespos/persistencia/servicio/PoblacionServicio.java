package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Poblacion;

public interface PoblacionServicio {
	public void crear(Poblacion p);
	public void modificar(Poblacion p);
	public void eliminar(Poblacion p);
	public Poblacion obtenerPorId(Integer id);
	public List<Poblacion> obtener();
}
