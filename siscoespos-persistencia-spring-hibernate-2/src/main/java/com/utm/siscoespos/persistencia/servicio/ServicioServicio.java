package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Servicio;

public interface ServicioServicio {
	public void crear(Servicio p);
	public void modificar(Servicio p);
	public void eliminar(Servicio p);
	public Servicio obtenerPorId(Integer id);
	public List<Servicio> obtener();
}
