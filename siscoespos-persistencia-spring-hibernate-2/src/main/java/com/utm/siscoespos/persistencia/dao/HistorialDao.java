package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Historial;

public interface HistorialDao {
	public void crear(Historial h);
	public void modificar(Historial h);
	public void eliminar(Historial h);
	public Historial obtenerPorId(Integer id);
	public Historial obtenerPorAnio(Integer anio);
	public Historial obtenerPorGrado(Integer grado);
	public List<Historial> obtener();
}
