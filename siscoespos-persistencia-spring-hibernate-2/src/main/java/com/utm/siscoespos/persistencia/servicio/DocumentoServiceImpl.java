package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.DocumentoDao;
import com.utm.siscoespos.persistencia.dominio.Documento;

@Transactional
@Named
public class DocumentoServiceImpl implements DocumentoService {

	@Inject
	private DocumentoDao documentoDao;

	@Override
	public List<Documento> obtenerDocumentos() {
		return documentoDao.obtenerDocumentos();
	}
	
	@Override
	public Documento obtenerDocumentoPorId(Integer idDocumento) {
		return documentoDao.obtenerDocumentoPorId(idDocumento);
	}

	
	@Override
	public void actualizarDocumento(Documento documento) {			
		documentoDao.actualizarDocumento(documento);
	}

	@Override
	public void crearDocumento(Documento documento) {
		documentoDao.crearDocumento(documento);
	}

	@Override
	public void eliminarDocumento(Documento documento) {
		documentoDao.eliminarDocumento(documento);
	}


}
