package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.HistorialDao;
import com.utm.siscoespos.persistencia.dominio.Historial;
@Transactional
@Named
public class HistorialServicioImpl implements HistorialServicio {
	@Inject
	HistorialDao historialDao;
	
	@Override
	public void crear(Historial p) {
		historialDao.crear(p);
	}

	@Override
	public void modificar(Historial p) {
		historialDao.modificar(p);
	}

	@Override
	public void eliminar(Historial p) {
		historialDao.eliminar(p);
	}

	@Override
	public Historial obtenerPorId(Integer id) {
		return historialDao.obtenerPorId(id);
	}

	@Override
	public List<Historial> obtener() {
		return historialDao.obtener();
	}

}
