package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Historial;
import com.utm.siscoespos.persistencia.dominio.Perro;

@Named
public class HistorialDaoImpl implements HistorialDao {
	
	@Inject 
	private SessionFactory sessionFactory;

	@Override
	public void crear(Historial h) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(h);
	}

	@Override
	public void modificar(Historial h) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(h);
	}

	@Override
	public void eliminar(Historial h) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(h);
	}

	@Override
	public Historial obtenerPorId(Integer id) {
		Historial historial = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Historial h where h.id=:id");
		query.setParameter("id", id);
		historial =(Historial) query.uniqueResult();
		return historial;
	}

	@Override
	public Historial obtenerPorAnio(Integer anio) {
		Historial historial = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Historial h where h.anio=:anio");
		query.setParameter("anio", anio);
		historial =(Historial) query.uniqueResult();
		return historial;
	}

	@Override
	public Historial obtenerPorGrado(Integer grado) {
		Historial historial = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Historial h where h.grado=:grado");
		query.setParameter("grado", grado);
		historial =(Historial) query.uniqueResult();
		return historial;
	}

	@Override
	public List<Historial> obtener() {
		List<Historial> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Historial h");		
		lista = query.list();
		
		return lista;
	}

}
