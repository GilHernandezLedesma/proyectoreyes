package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Documento;



public interface DocumentoService {
	
	List<Documento> obtenerDocumentos();
	
	Documento obtenerDocumentoPorId(Integer idDocumento);
	
	void actualizarDocumento(Documento Documento);	
	
	void crearDocumento(Documento Documento);
	
	void eliminarDocumento(Documento Documento);
}
