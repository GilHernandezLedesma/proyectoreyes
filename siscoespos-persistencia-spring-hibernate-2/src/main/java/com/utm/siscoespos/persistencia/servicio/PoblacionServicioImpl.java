package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.PoblacionDao;
import com.utm.siscoespos.persistencia.dominio.Poblacion;
@Transactional
@Named
public class PoblacionServicioImpl implements PoblacionServicio {
	@Inject
	PoblacionDao poblacionDao;
	
	@Override
	public void crear(Poblacion p) {
		poblacionDao.crear(p);
	}

	@Override
	public void modificar(Poblacion p) {
		poblacionDao.modificar(p);
	}

	@Override
	public void eliminar(Poblacion p) {
		poblacionDao.eliminar(p);
	}

	@Override
	public Poblacion obtenerPorId(Integer id) {
		return poblacionDao.obtenerPorId(id);
	}

	@Override
	public List<Poblacion> obtener() {
		return poblacionDao.obtener();
	}

}
