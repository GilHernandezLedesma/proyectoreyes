package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.PerroDao;
import com.utm.siscoespos.persistencia.dominio.Perro;
@Transactional
@Named
public class PerroServicioImpl implements PerroServicio {
	@Inject
	PerroDao perroDao;
	
	@Override
	public void crear(Perro p) {
		perroDao.crear(p);
	}

	@Override
	public void modificar(Perro p) {
		perroDao.modificar(p);
	}

	@Override
	public void eliminar(Perro p) {
		perroDao.eliminar(p);
	}

	@Override
	public Perro obtenerPorId(Integer id) {
		// TODO Auto-generated method stub
		return perroDao.obtenerPorId(id);
	}

	@Override
	public List<Perro> obtener() {
		// TODO Auto-generated method stub
		return perroDao.obtener();
	}

}
