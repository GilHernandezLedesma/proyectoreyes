package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Juguete;

@Named
public class JugueteDaoImpl implements JugueteDao {
	
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Juguete p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Juguete p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Juguete p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Juguete obtenerPorId(String id) {		
		Juguete perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Juguete p where p.id=:id");
		query.setParameter("id", id);
		perro =(Juguete) query.uniqueResult();
		return perro;
	}
	
	@Override
	public Juguete obtenerPrimero() {		
		Juguete perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Juguete j");
		query.setMaxResults(1);
		perro =(Juguete) query.uniqueResult();
		return perro;
	}

	@Override
	public List<Juguete> obtener() {
		List<Juguete> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Juguete p");		
		lista = query.list();
		return lista;
	}

}
