package com.utm.siscoespos.persistencia.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Peticion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "carta_id")
	private Carta carta;
	
	private Integer cantidad;
	
	@ManyToOne
	@JoinColumn(name = "juguete_id")
	private Juguete juguete;

	
	
	public Peticion() {
		// TODO Auto-generated constructor stub
	}
	public Peticion(Peticion p) {
		this.id=p.id;
		//this.carta=p.carta;
		this.cantidad=p.cantidad;
		//this.juguete=p.juguete;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Carta getCarta() {
		return carta;
	}

	public void setCarta(Carta carta) {
		this.carta = carta;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Juguete getJuguete() {
		return juguete;
	}

	public void setJuguete(Juguete juguete) {
		this.juguete = juguete;
	}

	
}
