package com.utm.siscoespos.persistencia.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Inventario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer cantidad;
	
	@ManyToOne
	@JoinColumn(name = "almacen_id")
	private Almacen almacen;

	@ManyToOne
	@JoinColumn(name = "juguete_id")
	private Juguete juguete;

	
	public Inventario() {
		// TODO Auto-generated constructor stub
	}
	public Inventario(Inventario i) {
		this.id=i.id;
		this.cantidad=i.cantidad;
		this.almacen=i.almacen;
		this.juguete=i.juguete;
	}

	//Sets and gets
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Almacen getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}
	public Juguete getJuguete() {
		return juguete;
	}
	public void setJuguete(Juguete juguete) {
		this.juguete = juguete;
	}
	
}
