package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Historial {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer anio;
	private Integer grado;
	
	@ManyToOne
	@JoinColumn(name = "nino_id")
	private Nino nino;

	@OneToMany(
			mappedBy = "historial", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Carta> cartas = new ArrayList<Carta>();

	
	@OneToMany(
			mappedBy = "historial", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Asignacion> asignaciones = new ArrayList<Asignacion>();


	public void addCarta(Carta c) {
		cartas.add(c);
		c.setNumCarta(cartas.size());
		c.setHistorial(this);
	}
	public void removeCarta(Carta c){
		cartas.remove(c);
		c.setHistorial(null);
	}
	
	public void addAsignacion(Asignacion a) {
		asignaciones.add(a);
		a.setHistorial(this);
	}
	public void removeAsignacion(Asignacion a){
		asignaciones.remove(a);
		a.setHistorial(null);
	}
	
	
	public Historial() {
	}
	public Historial(Historial h) {
		this.id = h.id;
		this.anio = h.anio;
		this.grado = h.grado;
		this.nino = h.nino;
		this.cartas = h.cartas;
		this.asignaciones = h.asignaciones;
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getAnio() {
		return anio;
	}


	public void setAnio(Integer anio) {
		this.anio = anio;
	}


	public Integer getGrado() {
		return grado;
	}


	public void setGrado(Integer grado) {
		this.grado = grado;
	}


	public Nino getNino() {
		return nino;
	}


	public void setNino(Nino nino) {
		this.nino = nino;
	}


	public List<Carta> getCartas() {
		return cartas;
	}


	public void setCartas(List<Carta> cartas) {
		this.cartas = cartas;
	}


	public List<Asignacion> getAsignaciones() {
		return asignaciones;
	}


	public void setAsignaciones(List<Asignacion> asignaciones) {
		this.asignaciones = asignaciones;
	}

	
}
