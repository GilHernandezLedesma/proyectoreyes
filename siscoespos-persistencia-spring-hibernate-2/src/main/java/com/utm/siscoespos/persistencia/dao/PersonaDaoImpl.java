package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.DistinctRootEntityResultTransformer;

import com.utm.siscoespos.persistencia.dominio.Persona;

@Named
public class PersonaDaoImpl implements PersonaDao {

	@Inject
	private SessionFactory sessionFactory;
	
	@Override
	public List<Persona> obtenerPersonas() {	
		List<Persona> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Persona p left join fetch p.documentos");
		lista = DistinctRootEntityResultTransformer.INSTANCE.transformList(query.list());
		return lista;
	}
	
	@Override
	public Persona obtenerPersonaPorId(Integer idPersona) {
		Persona persona=null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Persona p left join fetch p.documentos where p.idPersona=:idPersona");
		query.setParameter("idPersona", idPersona);
		persona=(Persona) query.uniqueResult();
		return persona;
	}

	
	@Override
	public void actualizarPersona(Persona persona) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(persona);
	}

	@Override
	public void crearPersona(Persona persona) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(persona);
	}

	@Override
	public void eliminarPersona(Persona persona) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(persona);
	}


}
