package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.AlmacenDao;
import com.utm.siscoespos.persistencia.dominio.Almacen;
@Transactional
@Named
public class AlmacenServicioImpl implements AlmacenServicio {
	@Inject
	AlmacenDao almacenDao;
	
	@Override
	public void crear(Almacen p) {
		almacenDao.crear(p);
	}

	@Override
	public void modificar(Almacen p) {
		almacenDao.modificar(p);
	}

	@Override
	public void eliminar(Almacen p) {
		almacenDao.eliminar(p);
	}

	@Override
	public Almacen obtenerPorId(Integer id) {
		return almacenDao.obtenerPorId(id);
	}

	@Override
	public List<Almacen> obtener() {
		return almacenDao.obtener();
	}

}
