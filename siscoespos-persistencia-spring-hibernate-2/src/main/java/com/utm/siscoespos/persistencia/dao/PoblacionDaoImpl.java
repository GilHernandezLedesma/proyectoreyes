package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Poblacion;


@Named
public class PoblacionDaoImpl implements PoblacionDao{
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Poblacion p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Poblacion p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Poblacion p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Poblacion obtenerPorId(Integer id) {		
		Poblacion poblacion = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Poblacion p where p.id=:id");
		query.setParameter("id", id);
		poblacion =(Poblacion) query.uniqueResult();
		return poblacion;
	}

	@Override
	public List<Poblacion> obtener() {
		List<Poblacion> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Poblacion p");		
		lista = query.list();
		return lista;
	}

}
