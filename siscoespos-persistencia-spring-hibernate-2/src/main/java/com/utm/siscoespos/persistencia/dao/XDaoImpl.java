package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.DistinctRootEntityResultTransformer;

import com.utm.siscoespos.persistencia.dominio.X;

@Named
public class XDaoImpl implements XDao {

	@Inject
	private SessionFactory sessionFactory;
	
	@Override
	public List<X> obtenerXs() {	
		List<X> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from X");
		lista = query.list();
		return lista;
	}
	
	@Override
	public X obtenerXPorId(Integer idX) {
		X x=null;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from X where idX=:idX");
		query.setParameter("idX", idX);
		x=(X) query.uniqueResult();
		return x;
	}

	
	@Override
	public void actualizarX(X x) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(x);
	}

	@Override
	public void crearX(X x) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(x);
	}

	@Override
	public void eliminarX(X x) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(x);
	}


}
