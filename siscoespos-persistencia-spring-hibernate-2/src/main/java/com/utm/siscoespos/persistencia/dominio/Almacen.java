package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Almacen {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@OneToOne
	@JoinColumn(name = "poblacion_id")
	private Poblacion poblacion;
	
	// IMPLEMENTACIÓN ASOCIACIÓN OneToMany	
	@OneToMany(
			mappedBy = "almacen", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Servicio> servicios = new ArrayList<Servicio>();
	
	@OneToMany(
			mappedBy = "almacen", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Inventario> inventarios = new ArrayList<Inventario>();


	
	public void addServicio(Servicio s) {
		servicios.add(s);
		s.setAlmacen(this);
	}
	public void removeServicio(Servicio s){
		servicios.remove(s);
		s.setAlmacen(null);
	}
	
	public void addInventario(Inventario i) {
		inventarios.add(i);
		i.setAlmacen(this);
	}
	public void removeInventario(Inventario s){
		inventarios.remove(s);
		s.setAlmacen(null);
	}
	
	
	public Almacen() {
	}
	public Almacen(Almacen a) {
		this.id = a.id;
		this.poblacion=a.poblacion;
		this.servicios=a.servicios;
		this.inventarios=a.inventarios;
	}

	//Sets and gets
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Poblacion getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(Poblacion poblacion) {
		this.poblacion = poblacion;
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public List<Inventario> getInventarios() {
		return inventarios;
	}

	public void setInventarios(List<Inventario> inventarios) {
		this.inventarios = inventarios;
	}
	
	
}
