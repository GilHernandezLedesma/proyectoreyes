package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Poblacion;

public interface PoblacionDao {
	public void crear(Poblacion p);
	public void modificar(Poblacion p);
	public void eliminar(Poblacion p);
	public Poblacion obtenerPorId(Integer id);
	public List<Poblacion> obtener();
}
