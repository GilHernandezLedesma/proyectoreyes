package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Nino;

public interface NinoServicio {
	public void crear(Nino p);
	public void modificar(Nino p);
	public void eliminar(Nino p);
	public Nino obtenerPorId(Integer id);
	public List<Nino> obtener();
}
