package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Peticion;

public interface PeticionDao {
	public void crear(Peticion p);
	public void modificar(Peticion p);
	public void eliminar(Peticion p);
	public Peticion obtenerPorId(Integer id);
	public List<Peticion> obtener();
}
