package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Entrenador;

@Named
public class EntrenadorDaoImpl implements EntrenadorDao {
	
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Entrenador p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Entrenador p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Entrenador p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Entrenador obtenerPorId(Integer id) {		
		Entrenador perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Entrenador p where p.id=:id");
		query.setParameter("id", id);
		perro =(Entrenador) query.uniqueResult();
		return perro;
	}

	@Override
	public List<Entrenador> obtener() {
		List<Entrenador> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Entrenador p");		
		lista = query.list();
		return lista;
	}

}
