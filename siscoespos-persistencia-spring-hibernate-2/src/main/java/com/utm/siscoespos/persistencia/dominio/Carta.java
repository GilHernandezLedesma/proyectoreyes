package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Carta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer numCarta;
	private String formaEnvio;

	@ManyToOne
	@JoinColumn(name = "historial_id")
	private Historial historial;

	@OneToMany(
			mappedBy = "carta", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Peticion> peticiones = new ArrayList<Peticion>();
	
	
	
	public Carta(Carta carta) {
		this.id = carta.id;
		this.numCarta = carta.numCarta;
		this.formaEnvio = carta.formaEnvio;
		this.historial = carta.historial;
		this.peticiones = carta.peticiones;
	}
	public Carta() {
		// TODO Auto-generated constructor stub
	}
	
	
	public void addPeticion(Peticion p) {
		peticiones.add(p);
		p.setCarta(this);
	}
	
	public void removePeticion(Peticion p){
		peticiones.remove(p);
		p.setCarta(null);
	}
	
	
	
	public Historial getHistorial() {
		return historial;
	}
	public void setHistorial(Historial historial) {
		this.historial = historial;
	}
	public List<Peticion> getPeticiones() {
		return peticiones;
	}
	public void setPeticiones(List<Peticion> peticiones) {
		this.peticiones = peticiones;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getNumCarta() {
		return numCarta;
	}
	public void setNumCarta(Integer numCarta) {
		this.numCarta = numCarta;
	}
	public String getFormaEnvio() {
		return formaEnvio;
	}
	public void setFormaEnvio(String formaEnvio) {
		this.formaEnvio = formaEnvio;
	}
	
}
