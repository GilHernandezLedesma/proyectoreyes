package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Servicio;

public interface ServicioDao {
	public void crear(Servicio s);
	public void modificar(Servicio s);
	public void eliminar(Servicio s);
	public Servicio obtenerPorId(Integer id);
	public List<Servicio> obtener();
}
