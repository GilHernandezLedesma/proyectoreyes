package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.InventarioDao;
import com.utm.siscoespos.persistencia.dominio.Inventario;
@Transactional
@Named
public class InventarioServicioImpl implements InventarioServicio {
	@Inject
	InventarioDao inventarioDao;
	
	@Override
	public void crear(Inventario p) {
		inventarioDao.crear(p);
	}

	@Override
	public void modificar(Inventario p) {
		inventarioDao.modificar(p);
	}

	@Override
	public void eliminar(Inventario p) {
		inventarioDao.eliminar(p);
	}

	@Override
	public Inventario obtenerPorId(Integer id) {
		return inventarioDao.obtenerPorId(id);
	}

	@Override
	public List<Inventario> obtener() {
		return inventarioDao.obtener();
	}

}
