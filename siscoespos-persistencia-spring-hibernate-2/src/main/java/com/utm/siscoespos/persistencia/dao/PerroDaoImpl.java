package com.utm.siscoespos.persistencia.dao;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.utm.siscoespos.persistencia.dominio.Documento;
import com.utm.siscoespos.persistencia.dominio.Perro;

@Named
public class PerroDaoImpl implements PerroDao {
	
	@Inject 
	private SessionFactory sessionFactory;
	
	@Override
	public void crear(Perro p) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(p);
	}

	@Override
	public void modificar(Perro p) {
		Session session = sessionFactory.getCurrentSession();
		session.merge(p);
	}

	@Override
	public void eliminar(Perro p) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(p);
	}

	@Override
	public Perro obtenerPorId(Integer id) {		
		Perro perro = null;

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Perro p where p.id=:id");
		query.setParameter("id", id);
		perro =(Perro) query.uniqueResult();
		return perro;
	}

	@Override
	public List<Perro> obtener() {
		List<Perro> lista;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Perro p");		
		lista = query.list();
		return lista;
	}

}
