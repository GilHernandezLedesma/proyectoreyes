package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Poblacion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String poblacion;
	
	// IMPLEMENTACIÓN ASOCIACIÓN OneToOne
	@OneToOne(mappedBy = "poblacion", cascade = CascadeType.ALL, orphanRemoval = true)
	private Almacen almacen;
	
	private String pais;
	
	// IMPLEMENTACIÓN ASOCIACIÓN OneToMany	
	@OneToMany(
			mappedBy = "poblacion", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Servicio> servicios = new ArrayList<Servicio>();

	@OneToMany(
			mappedBy = "poblacion", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Nino> ninos = new ArrayList<Nino>();
	
	

	
	public void addServicio(Servicio s) {
		servicios.add(s);
		s.setPoblacion(this);
	}
	public void removeServicios(Servicio s){
		servicios.remove(s);
		s.setPoblacion(null);
	}
	
	public void addNino(Nino n) {
		ninos.add(n);
		n.setPoblacion(this);
	}
	public void removeNino(Nino n){
		servicios.remove(n);
		n.setPoblacion(null);
	}
	
	
	public Poblacion() {
	}
	public Poblacion(Poblacion p) {
		this.id = p.id;
		this.poblacion=p.poblacion;
		this.almacen=null;//p.almacen;
		this.pais=p.pais;
		this.servicios=null;//p.servicios;
		this.ninos=null;//p.ninos;
	}

	public List<Nino> getNino() {
		return ninos;
	}

	public void setNino(List<Nino> nino) {
		this.ninos = nino;
	}

	//Sets and gets
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public Almacen getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Almacen almacen) {
		this.almacen = almacen;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}
	
	
	
}
