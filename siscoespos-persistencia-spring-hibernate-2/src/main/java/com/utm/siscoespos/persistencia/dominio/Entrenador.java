package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Entrenador {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String nombre;
	private String domicilio;
	private Double cuota;

	// IMPLEMENTACIÓN ASOCIACIÓN OneToOne
	//@OneToOne(mappedBy = "entrenador", cascade = CascadeType.ALL, orphanRemoval = true)
	//private Perro perro;
	
//	public Perro getPerro() {
//		return perro;
//	}
//
//	public void setPerro(Perro perro) {
//		this.perro = perro;
//		if (this.perro != null) {
//			this.perro.setEntrenador(this);
//		} 
//	}
	
	// IMPLEMENTACIÓN ASOCIACIÓN OneToMany	
	@OneToMany(
			mappedBy = "entrenador", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Perro> perros = new ArrayList<Perro>();

	public List<Perro> getPerros() {
		return perros;
	}

	public void setPerros(List<Perro> perros) {
		this.perros = perros;
	}
					
	public void addPerro(Perro p) {
		perros.add(p);
		p.setEntrenador(this);
	}
	
	public void removePerro(Perro p){
		perros.remove(p);
		p.setEntrenador(null);
	}
	
	
	// @ManyToMany
	// private List<Perro> perros;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public Double getCuota() {
		return cuota;
	}

	public void setCuota(Double cuota) {
		this.cuota = cuota;
	}

}
