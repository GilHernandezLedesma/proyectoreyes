package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import com.utm.siscoespos.persistencia.dominio.Almacen;

public interface AlmacenServicio {
	public void crear(Almacen p);
	public void modificar(Almacen p);
	public void eliminar(Almacen p);
	public Almacen obtenerPorId(Integer id);
	public List<Almacen> obtener();
}
