package com.utm.siscoespos.persistencia.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Juguete {
	@Id
	private String nombre;
	private String descripcion;
	private Integer edadAutorizada;
	private String categoria;
	
	@OneToMany(
			mappedBy = "juguete", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Peticion> peticiones = new ArrayList<Peticion>();

	@OneToMany(
			mappedBy = "juguete", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Asignacion> asignaciones = new ArrayList<Asignacion>();

	@OneToMany(
			mappedBy = "juguete", 
			cascade = CascadeType.ALL, 
			orphanRemoval = true)
	private List<Inventario> inventarios = new ArrayList<Inventario>();

	
	public void addPeticion(Peticion p) {
		peticiones.add(p);
		p.setJuguete(this);
	}
	public void removePeticion(Peticion p){
		peticiones.remove(p);
		p.setJuguete(null);
	}
	
	public void addAsignacion(Asignacion a) {
		asignaciones.add(a);
		a.setJuguete(this);
	}
	public void removeAsignacion(Asignacion a){
		peticiones.remove(a);
		a.setJuguete(null);
	}
	
	public void addInventario(Inventario i) {
		inventarios.add(i);
		i.setJuguete(this);
	}
	public void removeInventarios(Inventario i){
		peticiones.remove(i);
		i.setJuguete(null);
	}
	
	
	public Juguete() {
		// TODO Auto-generated constructor stub
	}
	public Juguete(Juguete j) {
		this.nombre=j.nombre;
		this.descripcion=j.descripcion;
		this.edadAutorizada=j.edadAutorizada;
		this.categoria=j.categoria;
		this.peticiones=j.peticiones;
		this.asignaciones=j.asignaciones;
		this.inventarios=j.inventarios;
	}

	public List<Asignacion> getAsignaciones() {
		return asignaciones;
	}

	public void setAsignaciones(List<Asignacion> asignacion) {
		this.asignaciones = asignacion;
	}

	public List<Inventario> getInventarios() {
		return inventarios;
	}

	public void setInventarios(List<Inventario> inventarios) {
		this.inventarios = inventarios;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getEdadAutorizada() {
		return edadAutorizada;
	}

	public void setEdadAutorizada(Integer edadAutorizada) {
		this.edadAutorizada = edadAutorizada;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public List<Peticion> getPeticiones() {
		return peticiones;
	}

	public void setPeticiones(List<Peticion> peticiones) {
		this.peticiones = peticiones;
	}


	
}
