package com.utm.siscoespos.persistencia.servicio;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import com.utm.siscoespos.persistencia.dao.XDao;
import com.utm.siscoespos.persistencia.dominio.X;

@Transactional
@Named
public class XServiceImpl implements XService {

	@Inject
	private XDao xDao;

	@Override
	public List<X> obtenerXs() {
		return xDao.obtenerXs();
	}
	
	@Override
	public X obtenerXPorId(Integer idX) {
		return xDao.obtenerXPorId(idX);
	}

	
	@Override
	public void actualizarX(X x) {			
		xDao.actualizarX(x);
	}

	@Override
	public void crearX(X x) {
		xDao.crearX(x);
	}

	@Override
	public void eliminarX(X x) {
		xDao.eliminarX(x);
	}


}
