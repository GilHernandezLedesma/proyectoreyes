package com.utm.siscoespos;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dominio.Almacen;
import com.utm.siscoespos.persistencia.dominio.Asignacion;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.dominio.Historial;
import com.utm.siscoespos.persistencia.dominio.Inventario;
import com.utm.siscoespos.persistencia.dominio.Juguete;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.dominio.Peticion;
import com.utm.siscoespos.persistencia.dominio.Poblacion;
import com.utm.siscoespos.persistencia.dominio.Servicio;
import com.utm.siscoespos.persistencia.servicio.AlmacenServicio;
import com.utm.siscoespos.persistencia.servicio.AsignacionServicio;
import com.utm.siscoespos.persistencia.servicio.CartaServicio;
import com.utm.siscoespos.persistencia.servicio.HistorialServicio;
import com.utm.siscoespos.persistencia.servicio.InventarioServicio;
import com.utm.siscoespos.persistencia.servicio.JugueteServicio;
import com.utm.siscoespos.persistencia.servicio.NinoServicio;
import com.utm.siscoespos.persistencia.servicio.PeticionServicio;
import com.utm.siscoespos.persistencia.servicio.PoblacionServicio;
import com.utm.siscoespos.persistencia.servicio.ServicioServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class DeleteTest {
	
	@Inject
	private CartaServicio cartaServicio;   
	@Inject
	private HistorialServicio historialServicio;   
	@Inject
	private NinoServicio ninoServicio;  
	@Inject
	private PoblacionServicio poblacionServicio; 
	@Inject
	private ServicioServicio servicioServicio; 
	@Inject
	private AlmacenServicio almacenServicio; 
	@Inject
	private InventarioServicio inventarioServicio;
	@Inject
	private JugueteServicio jugueteServicio;
	@Inject
	private AsignacionServicio asignacionServicio;
	@Inject
	private PeticionServicio peticionServicio;
	
	private Integer ID = 17;
	
	@Test
	public void delete(){
		
		Asignacion asignacion = asignacionServicio.obtenerPorId(ID);
		System.out.println("Asignacion: ");
		System.out.println("    id:" + asignacion.getId() + "|juguete:" + asignacion.getJuguete().getNombre() + "|ninio:" + asignacion.getHistorial().getNino().getNombre() + "\n");
		asignacionServicio.eliminar(asignacion);
		
		Peticion peticion = peticionServicio.obtenerPorId(ID);
		System.out.println("Peticion: ");
		System.out.println("    id:" + peticion.getId() + "|juguete:" + peticion.getJuguete().getNombre() + "|ninio:" + peticion.getCarta().getHistorial().getNino().getNombre() + "\n");
		peticionServicio.eliminar(peticion);
		
		Carta carta = cartaServicio.obtenerPorId(ID);
		System.out.println("Carta: ");
		System.out.println("    id:" + carta.getId() + "|envio:" + carta.getFormaEnvio() + "|anio:" + carta.getHistorial().getAnio() + "\n");
		cartaServicio.eliminar(carta);
		
		Historial historial = historialServicio.obtenerPorId(ID);
		System.out.println("Historial: ");
		System.out.println("    id:" + historial.getId() + "|anio:" + historial.getAnio() + "|nino:" + historial.getNino().getNombre() + "\n");
		historialServicio.eliminar(historial);
		
		Nino nino = ninoServicio.obtenerPorId(ID);
		System.out.println("Ninio: ");
		System.out.println("    id:" + nino.getId() + "|direccion:" + nino.getDireccion() + "|nombre:" + nino.getNombre() + "\n");
		ninoServicio.eliminar(nino);
		
		Servicio servicio = servicioServicio.obtenerPorId(ID);
		System.out.println("Servicio: ");
		System.out.println("    id:" + servicio.getId() + "|almacen_id:" + servicio.getAlmacen().getId() + "\n");
		servicioServicio.eliminar(servicio);
		
		Inventario inventario = inventarioServicio.obtenerPorId(ID);
		System.out.println("Inventario: ");
		System.out.println("    id:" + inventario.getId() + "|juguete:" + inventario.getJuguete().getNombre() + "|cantidad:" + inventario.getCantidad() + "\n");
		inventarioServicio.eliminar(inventario);
		
		Almacen almacen = almacenServicio.obtenerPorId(ID);
		System.out.println("Almacen: ");
		System.out.println("    id:" + almacen.getId() + "|poblacion:" + almacen.getPoblacion().getPoblacion() + "\n");
		almacenServicio.eliminar(almacen);
		
		Poblacion poblacion = poblacionServicio.obtenerPorId(ID);
		System.out.println("Poblacion: ");
		System.out.println("    id:" + poblacion.getId() + "|poblacion:" + poblacion.getPoblacion() + "|pais:" + poblacion.getPais() + "\n");
		poblacionServicio.eliminar(poblacion);
		
		Juguete juguete = jugueteServicio.obtenerPrimero();
		System.out.println("Juguete: ");
		System.out.println("    nombre:" + juguete.getNombre() + "|categoria:" + juguete.getCategoria() + "|descripcion:" + juguete.getDescripcion() + "\n");
		jugueteServicio.eliminar(juguete);
		
	}

}
