package com.utm.siscoespos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dominio.Perro;
import com.utm.siscoespos.persistencia.servicio.PerroServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class PerroServicioTest {
	
	@Inject
	private PerroServicio perroServicio;
	
		
	@Test
	public void crearPerro() {
				
		Perro p = new Perro();
		p.setNombre("Firulais");
		p.setRaza("Boxer");
		p.setFechaNacimiento(new Date());
		p.setAlto(20.1);
		perroServicio.crear(p);
	}
	

}
