package com.utm.siscoespos;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.utm.siscoespos.persistencia.dominio.Almacen;
import com.utm.siscoespos.persistencia.dominio.Asignacion;
import com.utm.siscoespos.persistencia.dominio.Carta;
import com.utm.siscoespos.persistencia.dominio.Historial;
import com.utm.siscoespos.persistencia.dominio.Inventario;
import com.utm.siscoespos.persistencia.dominio.Juguete;
import com.utm.siscoespos.persistencia.dominio.Nino;
import com.utm.siscoespos.persistencia.dominio.Peticion;
import com.utm.siscoespos.persistencia.dominio.Poblacion;
import com.utm.siscoespos.persistencia.dominio.Servicio;
import com.utm.siscoespos.persistencia.servicio.AlmacenServicio;
import com.utm.siscoespos.persistencia.servicio.AsignacionServicio;
import com.utm.siscoespos.persistencia.servicio.CartaServicio;
import com.utm.siscoespos.persistencia.servicio.HistorialServicio;
import com.utm.siscoespos.persistencia.servicio.InventarioServicio;
import com.utm.siscoespos.persistencia.servicio.JugueteServicio;
import com.utm.siscoespos.persistencia.servicio.NinoServicio;
import com.utm.siscoespos.persistencia.servicio.PeticionServicio;
import com.utm.siscoespos.persistencia.servicio.PoblacionServicio;
import com.utm.siscoespos.persistencia.servicio.ServicioServicio;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml"})
public class MiTest {
	@Inject
	private PoblacionServicio pobServicio; 
	@Inject
	private AlmacenServicio almServicio; 
	@Inject
	private ServicioServicio serServicio; 
	@Inject
	private NinoServicio ninoServicio; 
	@Inject
	private HistorialServicio histServicio;
	@Inject 
	private CartaServicio cartaServicio;
	@Inject 
	private PeticionServicio petServicio;
	@Inject
	private AsignacionServicio asigServicio;
	@Inject
	private JugueteServicio jugueteServicio;
	@Inject
	private InventarioServicio inventarioServicio;
	@Test
	public void crudPoblacion(){
		Poblacion pob = new Poblacion();
		pob.setPais("México");
		pob.setPoblacion("Santa Lucia del Camino");
		
		pobServicio.crear(pob);
		
		Almacen almacen = new Almacen();
		almacen.setPoblacion(pob);
		
		almServicio.crear(almacen);
		
				
		Poblacion pob2 = new Poblacion();
		pob2.setPais("México");
		pob2.setPoblacion("Xoxocotlan");
		//pob2.setServicios(null);
		//pob2.setAlmacen(null);
		//pob2.setNino(null);
		
		pobServicio.crear(pob2);
		
		Almacen almacen2 = new Almacen();
		almacen2.setPoblacion(pob2);
		//almacen2.setInventarios(null);
		//almacen2.setServicios(null);

		almServicio.crear(almacen2);
		
		

		Servicio servicio = new Servicio();
		servicio.setAlmacen(almacen);
		servicio.setPoblacion(pob);
		
		serServicio.crear(servicio);
		
		Servicio servicio2 = new Servicio();
		servicio2.setAlmacen(almacen2);
		servicio2.setPoblacion(pob2);

		serServicio.crear(servicio2);
		
		Nino nino = new Nino();
		nino.setDireccion("Solar # 32 Col. Villas Azul");
		nino.setFechaNacimiento(new Date());
		//nino.setHistoriales(null);
		nino.setNombre("Gil Hernández");
		nino.setPoblacion(pob);
		
		
		Nino nino2 = new Nino();
		nino2.setDireccion("Universo # 777 Col. Violetas");
		nino2.setFechaNacimiento(new Date());
		//nino2.setHistoriales(null);
		nino2.setNombre("Pedro La Piedra");
		nino2.setPoblacion(pob2);

		ninoServicio.crear(nino);
		ninoServicio.crear(nino2);
		
		Historial h1 = new Historial();
		h1.setAnio(2017);
		
		//h1.setAsignaciones(null);
		//h1.setCartas(null);
		h1.setGrado(10);
		h1.setNino(nino);
		
		
		Historial h2 = new Historial();
		h2.setAnio(2017);
		//h2.setAsignaciones(null);
		//h2.setCartas(null);
		h2.setGrado(7);
		h2.setNino(nino2);
		

		histServicio.crear(h1);
		histServicio.crear(h2);
		
		
		Carta c1 = new Carta();
		c1.setFormaEnvio("email");
		c1.setHistorial(h1);
		c1.setNumCarta(1);
//		c1.setPeticiones(null);
		
		Carta c2 = new Carta();
		c2.setFormaEnvio("correo postal");
		c2.setHistorial(h2);
		c2.setNumCarta(1);
//		c2.setPeticiones(null);

		cartaServicio.crear(c1);
		cartaServicio.crear(c2);
		
		Peticion pet = new Peticion();
		pet.setCantidad(1);
		pet.setCarta(c1);
		

		Peticion pet2 = new Peticion();
		pet2.setCantidad(2);
		pet2.setCarta(c2);

		petServicio.crear(pet);
		petServicio.crear(pet2);
		
		Asignacion asig = new Asignacion();
		asig.setHistorial(h1);
		
		Asignacion asig2 = new Asignacion();
		asig2.setHistorial(h2);

		asigServicio.crear(asig);
		asigServicio.crear(asig2);
		
		
		Juguete jug1 = new Juguete();
		jug1.setCategoria("carro");
		//jug1.addAsignacion(asig);
		jug1.setDescripcion("Un hot wheels rojo");
		jug1.setEdadAutorizada(10);
		jug1.setNombre("Mazda"+Math.random());
//		jug1.getPeticiones().add(pet);
		
		
		//petServicio.modificar(pet);
		
		jugueteServicio.crear(jug1);
		jug1.addAsignacion(asig);
		jug1.addPeticion(pet);
		asigServicio.modificar(asig);
		petServicio.modificar(pet);
		jugueteServicio.modificar(jug1);
		
		Juguete jug2 = new Juguete();
		jug2.setCategoria("carro");
		jug2.setDescripcion("Un mattel amarillo");
		jug2.setEdadAutorizada(11);
		jug2.setNombre("Spark"+Math.random());
		
		jugueteServicio.crear(jug2);
		jug2.addAsignacion(asig2);
		jug2.addPeticion(pet2);
		asigServicio.modificar(asig2);
		petServicio.modificar(pet2);
		jugueteServicio.modificar(jug2);
		
		Inventario in = new Inventario();
		in.setAlmacen(almacen);
		in.setCantidad(10);
		
		Inventario in2 = new Inventario();
		in2.setAlmacen(almacen2);
		in2.setCantidad(11);

		inventarioServicio.crear(in);
		inventarioServicio.crear(in2);
		
		
		pob.setAlmacen(almacen);
		pob.addNino(nino);
		pob.addServicio(servicio);
		ninoServicio.modificar(nino);
		serServicio.modificar(servicio);
		pobServicio.modificar(pob);
		
		pob2.setAlmacen(almacen2);
		pob2.addNino(nino2);
		pob2.addServicio(servicio2);
		ninoServicio.modificar(nino2);
		serServicio.modificar(servicio2);
		pobServicio.modificar(pob2);
		
		
		almacen.addInventario(in);
		almacen.addServicio(servicio);
		almServicio.modificar(almacen);
		inventarioServicio.modificar(in);
		serServicio.modificar(servicio);
		
		almacen2.addInventario(in2);
		almacen2.addServicio(servicio2);
		almServicio.modificar(almacen2);	
		inventarioServicio.modificar(in2);
		serServicio.modificar(servicio2);
		
		
		nino.addHistorial(h1);
		ninoServicio.modificar(nino);
		
		nino2.addHistorial(h2);
		ninoServicio.modificar(nino2);

		h1.addAsignacion(asig);
		h1.addCarta(c1);
		histServicio.modificar(h1);
		asigServicio.modificar(asig);
		
		h2.addAsignacion(asig2);
		h2.addCarta(c2);
		histServicio.modificar(h2);
		asigServicio.modificar(asig2);
		
		c1.addPeticion(pet);
		cartaServicio.modificar(c1);
		c2.addPeticion(pet2);
		cartaServicio.modificar(c2);

		jug1.addInventario(in);
		jugueteServicio.modificar(jug1);
		
		jug2.addInventario(in2);
		jugueteServicio.modificar(jug2);
		/*
		c1.getPeticiones().add(pet);
		c2.getPeticiones().add(pet2);		
		jug1.getInventarios().add(in);
		jug2.getInventarios().add(in2);

*/
	}
}
